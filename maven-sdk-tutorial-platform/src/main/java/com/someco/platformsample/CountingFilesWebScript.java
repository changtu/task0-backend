/**
 * Copyright (C) 2017 Alfresco Software Limited.
 * <p/>
 * This file is part of the Alfresco SDK project.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.someco.platformsample;

import com.google.gson.reflect.TypeToken;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.search.CategoryService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.namespace.RegexQNamePattern;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.Cache;
import org.springframework.extensions.webscripts.DeclarativeWebScript;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.WebScriptRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A demonstration Java controller for the Hello World Web Script.
 *
 * @author martin.bergljung@alfresco.com
 * @since 2.1.0
 */
public class CountingFilesWebScript extends DeclarativeWebScript {
    private static Log logger = LogFactory.getLog(CountingFilesWebScript.class);
    @Autowired
    private DictionaryService dictionaryService;
    @Autowired
    private NodeService nodeService;

    public class Result {
        public String nodeId;
        public int filesCount;
        public int foldersCount;
//        public static int totalCount;
    }


    protected Map<String, Object> executeImpl(
            WebScriptRequest req, Status status, Cache cache) {
        String nodeId = req.getParameter("id");
        logger.info("Counting files in node " + nodeId);
        NodeRef nodeRef = new NodeRef("workspace", "SpacesStore", nodeId);
        logger.info("NodeRef: " + nodeRef.toString());

        Map<String, Object> model = new HashMap<String, Object>();
        Result result = new Result();
        // Get the number of files in the /files folder
        result.filesCount = 0;
        result.foldersCount = 0;
        try {
            getFilesCount(nodeRef, result);
        } catch (Exception e) {
            logger.error("Error counting files", e);
        }
        // Add the number of files to the model
        model.put("filesCount", result.filesCount);
        // Add the number of folders to the model
        model.put("foldersCount", result.foldersCount);
        logger.info("NodeId: " + nodeId + " filesCount: " + result.filesCount + " foldersCount: " + result.foldersCount);
        logger.debug("Your 'Couting Files' Web Script was called!");
        return model;
    }

    /**
     * @param type
     * @param nodeRef
     * @return true if node type equals or inherit from type <code>type</code>
     */
    protected boolean hasSubType(QName type, NodeRef nodeRef) {
        logger.info("Checking if node " + nodeRef.toString() + " has subtype " + type.toString());
        List<QName> subTypes = new ArrayList<>();
        subTypes.addAll(dictionaryService.getSubTypes(type, true));
        QName nodeType = nodeService.getType(nodeRef);
        return nodeType.equals(type) || subTypes.contains(nodeType);
    }

    /**
     * @param nodeRef
     * @param result  The result object contains the counts of files and folders
     * @return number of files in the nodeRef folder
     */
    private void getFilesCount(NodeRef nodeRef, Result result) {
//        logger.info("Counting files in nodeRef: " + nodeRef);
        if (hasSubType(ContentModel.TYPE_FOLDER, nodeRef)) {
            List<ChildAssociationRef> children = nodeService.getChildAssocs(nodeRef, ContentModel.ASSOC_CONTAINS, RegexQNamePattern.MATCH_ALL);
            for (ChildAssociationRef child : children) {
                NodeRef childNode = child.getChildRef();
                if (hasSubType(ContentModel.TYPE_CONTENT, childNode)) {
                    result.filesCount++;
                } else if (hasSubType(ContentModel.TYPE_FOLDER, childNode)) {
//                    countFolder++;
                    result.foldersCount++;
                    getFilesCount(childNode, result);
                }
            }
        } else {
        }
    }
}