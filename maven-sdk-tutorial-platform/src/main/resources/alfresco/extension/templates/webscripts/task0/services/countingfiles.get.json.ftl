<#escape x as jsonUtils.encodeJSONString(x)>
    {
    "nodeid": "${args.id}" <#if filesCount??>,
    "filesCount": "${filesCount}" </#if> <#if foldersCount??>,
    "foldersCount": "${foldersCount}"
    </#if>
    }
</#escape>